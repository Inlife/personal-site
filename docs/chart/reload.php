<?php   
 /*
     Example1 : A simple line chart
 */

 // Standard inclusions      
 include("pChart/pData.class");   
 include("pChart/pChart.class");   
  
 // Dataset definition    
 $DataSet1 = new pData;   
 $DataSet1->ImportFromCSV("stats/stock.txt",",",array(1),TRUE,0);   
 $DataSet1->AddAllSeries();   
 $DataSet1->SetAbsciseLabelSerie();   
 $DataSet1->SetSerieName("INLIFE CORP","Serie1");   
 $DataSet1->SetYAxisUnit(" gold");
  
 // Initialise the graph   
 $Test1 = new pChart(700,230);
 $Test1->setFontProperties("Fonts/tahoma.ttf",8);   
 $Test1->setGraphArea(70,30,680,200);   
 $Test1->drawFilledRoundedRectangle(7,7,693,223,5,240,240,240);   
 $Test1->drawRoundedRectangle(5,5,695,225,5,230,230,230);   
 $Test1->drawGraphArea(255,255,255,TRUE);
 $Test1->drawScale($DataSet1->GetData(),$DataSet1->GetDataDescription(),SCALE_NORMAL,150,150,150,TRUE,0,2);   
 $Test1->drawGrid(4,TRUE,230,230,230,50);
  
 // Draw the 0 line   
 $Test1->setFontProperties("Fonts/tahoma.ttf",6);   
 $Test1->drawTreshold(0,143,55,72,TRUE,TRUE);   
  
 // Draw the line graph
 $Test1->drawLineGraph($DataSet1->GetData(),$DataSet1->GetDataDescription());   
 $Test1->drawPlotGraph($DataSet1->GetData(),$DataSet1->GetDataDescription(),3,2,255,255,255);   
  
 // Finish the graph   
 $Test1->setFontProperties("Fonts/tahoma.ttf",8);   
 $Test1->drawLegend(75,35,$DataSet1->GetDataDescription(),255,255,255);   
 $Test1->setFontProperties("Fonts/tahoma.ttf",10);   
 $Test1->drawTitle(60,22,"Stock Exchange",50,50,50,585);   
 $Test1->Render("stock.png");
 
 /*
     Example10 : A 3D exploded pie graph
 */

 // Standard inclusions   

 // Dataset definition 
 $DataSet = new pData;
 $Text = explode(";", file_get_contents("stats/investors.txt"));
 $data0 = explode(",", $Text[0]);
 $data1 = explode(",", $Text[1]);
 $DataSet->AddPoint($data1,"Serie1");
 $DataSet->AddPoint($data0,"Serie2");
 $DataSet->AddAllSeries();
 $DataSet->SetAbsciseLabelSerie("Serie2");

 // Initialise the graph
 $Test = new pChart(420,250);
 $Test->drawFilledRoundedRectangle(7,7,413,243,5,240,240,240);
 $Test->drawRoundedRectangle(5,5,415,245,5,230,230,230);
 $Test->createColorGradientPalette(195,204,56,223,110,41,5);

 // Draw the pie chart
 $Test->setFontProperties("Fonts/tahoma.ttf",8);
 $Test->AntialiasQuality = 0;
 $Test->drawPieGraph($DataSet->GetData(),$DataSet->GetDataDescription(),180,130,110,PIE_PERCENTAGE_LABEL,FALSE,50,20,5);
 $Test->drawPieLegend(330,15,$DataSet->GetData(),$DataSet->GetDataDescription(),250,250,250);

 // Write the title
 $Test->setFontProperties("Fonts/MankSans.ttf",10);
 $Test->drawTitle(10,20,"Biggest Stock Owners",100,100,100);
 $Test->Render("investors.png");
 
 $string = file_get_contents("stats/per_price.txt");
  $len = strlen($string) * 3;
  $i = imageCreate(50, 25);
  $color = imageColorAllocate($i, 255, 255, 255);
  $textcolor = imageColorAllocate($i, 0, 0, 0);
  imagerectangle($i, 0, 0, imageSX($i) - 1, imageSY($i) - 1, $textcolor);
  imagestring($i, 3, 23 - $len, 5, $string, $textcolor); 
  imageJpeg($i, "price.jpg");
  imageDestroy($i); 
 
 
 ?>