<?php

defined('_ENGINE') or die('No access');

###############

function page_name($p) {
  switch($p) {
    case 1: $PAGE = "news"; $t = "News"; break;
    case 2: $PAGE = "about"; $t = "About"; break;
    case 3: $PAGE = "projects"; $t = "Projects"; break;
    default: $PAGE = ''; $t = "Main"; break;
  }  
  $T = 'Inlife Software | '.$t;
  $RETURN = array($PAGE, $T);
  return $RETURN;
}

##############

function redirect($url,$time = 0) {
  print "<meta http-equiv='refresh' content='$time; URL=$url'>";
}

##############

function set_window($txt, $page) {
  $ALL = '<div allign="center" class="big_center">';
  $array = explode("\n", $txt);
  $Rarray = krsort($array);

  foreach ($array as $key => $value) {
    $str = explode(",", $value);
    if (count($str) > 3) {
      $date = $str[3];
    }else{
      $date = '';
    }
    $text = '<div class="small_block"><br />
      <table width="684" height="193" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td align="left" width="684" height="35" class="window_title"><a href="/full-page/'.$page.'-'.$key.'">'.$str[0].'</a></td>
        </tr>
        <tr>
          <td align="left" height="120" valign="top" class="window_text"><hr />'.$str[1].'</td>
        </tr>
        <tr>
          <td align="left" height="24" class="window_date"><hr />'.$date.'</td>
        </tr>
      </table>
    </div>';
    $ALL .= $text;
  }
  return $ALL."</div>";
}

##############

function set_full($PAGE, $P, $ID) {
  $content = file('content/'.$PAGE.".txt");
  $str = explode(",", $content[$ID]);
  if (count($str) > 3) {
      $date = "<hr />".$str[3].'<hr />';
      $like = '<script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="" show_faces="false" width="450" font="verdana" colorscheme="dark"></fb:like>';
    }else{
      $date = '';
      $like = '';
    }
    $C = '<div class="big_center">
    <div class="center_block"><br />
      <table width="684" height="600" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td align="left" width="684" height="30" class="window_title">'.$str[0].'</td>
        </tr>
        <tr>
          <td align="left"  valign="top" class="window_text"><hr />'.$str[2].'</td>
        </tr>
        <tr height="24">
          <td align="left"  class="window_date">'.$date.$like.'</td>
        </tr>
        <tr height="200">
          <td align="center" valign="top" class="window_date"><hr /><iframe frameborder="0" src="http://inlife.no-ip.org/comment.php?page='.$P.'&id='.$ID.'" width="680" height="300"></iframe></td>
        </tr>
      </table>
    </div>
   </div>';
   return $C;
}   

##############
?>